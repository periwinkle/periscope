mod config_skin;
mod display_skin;

pub use config_skin::StickMode;
pub use display_skin::{ButtonType, PlayerSkin, Skin};

use crate::config::config_dir;

use anyhow::{Context, Result};
use image::io::Reader;
use macroquad::texture::Texture2D;
use serde::Deserialize;
use std::{
    fs,
    path::{Path, PathBuf},
};
use toml::from_str;

fn load_image<P: AsRef<Path>>(path: P) -> Result<Texture2D> {
    let img = Reader::open(path)
        .context("Couldn't open image file.")?
        .with_guessed_format()
        .context("Invalid image file.")?
        .decode()
        .context("Invalid image file.")?
        .into_rgba8();
    Ok(Texture2D::from_rgba8(
        img.width() as u16,
        img.height() as u16,
        img.as_raw(),
    ))
}

fn default_image() -> Texture2D {
    Texture2D::from_rgba8(1, 1, &[0, 0, 0, 0])
}

#[derive(Deserialize, Debug, Clone, Copy, Default)]
pub struct Pos {
    pub x: f32,
    pub y: f32,
}

#[derive(Deserialize, Debug)]
struct Background {
    background: String,
}

pub fn bg_dims(name: &str) -> Result<(i32, i32)> {
    let mut p = skin_path(name).join("skin.toml");
    let bg: Background = from_str(&fs::read_to_string(&p).context("Couldn't open skin.toml.")?)
        .context("Invalid skin.toml")?;
    p.pop();
    let i = Reader::open(p.join(bg.background))
        .context("Couldn't open background image file.")?
        .with_guessed_format()
        .context("Invalid background image file.")?
        .decode()
        .context("Invalid background image file.")?;
    Ok((i.width() as i32, i.height() as i32))
}

pub fn skin_path(name: &str) -> PathBuf {
    if name.ends_with(" (local)") {
        PathBuf::from("./skins/").join(name.strip_suffix(" (local)").unwrap())
    } else {
        config_dir().join(name)
    }
}
