use anyhow::{Context, Result};
use macroquad::texture::Texture2D;
use serde::Deserialize;
use std::{collections::HashMap, path::Path};

use super::{config_skin::*, default_image, load_image, skin_path, Pos};

impl Skin {
    pub fn open(name: &str) -> Result<Self> {
        let base = skin_path(name);
        let cfg = ConfigSkin::open(base.join("skin.toml"))?;
        Ok(Self {
            background: load_image(base.join(&cfg.background))
                .context("Couldn't load background image.")?,
            stick_mode: cfg.stick_mode,
            players: [
                PlayerSkin::from_cfg(cfg.player1, &base).context("Couldn't load player 1 skin.")?,
                PlayerSkin::from_cfg(cfg.player2, &base).context("Couldn't load player 2 skin.")?,
                PlayerSkin::from_cfg(cfg.player3, &base).context("Couldn't load player 3 skin.")?,
                PlayerSkin::from_cfg(cfg.player4, &base).context("Couldn't load player 4 skin.")?,
                PlayerSkin::from_cfg(cfg.player5, &base).context("Couldn't load player 5 skin.")?,
                PlayerSkin::from_cfg(cfg.player6, &base).context("Couldn't load player 6 skin.")?,
                PlayerSkin::from_cfg(cfg.player7, &base).context("Couldn't load player 7 skin.")?,
                PlayerSkin::from_cfg(cfg.player8, &base).context("Couldn't load player 8 skin.")?,
            ],
        })
    }
}

impl Default for Skin {
    fn default() -> Self {
        Self {
            background: default_image(),
            stick_mode: StickMode::StickOnTop,
            players: [
                PlayerSkin::default(),
                PlayerSkin::default(),
                PlayerSkin::default(),
                PlayerSkin::default(),
                PlayerSkin::default(),
                PlayerSkin::default(),
                PlayerSkin::default(),
                PlayerSkin::default(),
            ],
        }
    }
}

impl PlayerSkin {
    fn from_cfg(cfg: ConfigPlayer, base: &Path) -> Result<Self> {
        Ok(Self {
            buttons: buttons_from_cfg(&cfg.buttons, &base)?,
            ls: Stick::from_cfg(&cfg.ls, &base).context("Couldn't load left stick image.")?,
            rs: Stick::from_cfg(&cfg.rs, &base).context("Couldn't load right stick image.")?,
        })
    }
}

impl Default for PlayerSkin {
    fn default() -> Self {
        Self {
            buttons: HashMap::new(),
            ls: Stick::default(),
            rs: Stick::default(),
        }
    }
}

impl Stick {
    fn from_cfg(cfg: &ConfigStick, base: &Path) -> Result<Self> {
        Ok(Self {
            pos: cfg.pos,
            range: cfg.range,
            tex: if cfg.image.is_empty() {
                default_image()
            } else {
                load_image(base.join(&cfg.image))?
            },
        })
    }
}

impl Default for Stick {
    fn default() -> Self {
        Self {
            pos: Pos { x: 0.0, y: 0.0 },
            range: 0.0,
            tex: default_image(),
        }
    }
}

fn buttons_from_cfg(
    cfg: &ConfigButtons,
    base: &Path,
) -> Result<HashMap<ButtonType, ButtonDisplay>> {
    use ButtonType::*;
    let mut r = HashMap::new();
    r.insert(
        A,
        ButtonDisplay::from_cfg(&cfg.a, &base).context("Couldn't load A image.")?,
    );
    r.insert(
        B,
        ButtonDisplay::from_cfg(&cfg.b, &base).context("Couldn't load B image.")?,
    );
    r.insert(
        X,
        ButtonDisplay::from_cfg(&cfg.x, &base).context("Couldn't load X image.")?,
    );
    r.insert(
        Y,
        ButtonDisplay::from_cfg(&cfg.y, &base).context("Couldn't load Y image.")?,
    );
    r.insert(
        Plus,
        ButtonDisplay::from_cfg(&cfg.plus, &base).context("Couldn't load Plus image.")?,
    );
    r.insert(
        Minus,
        ButtonDisplay::from_cfg(&cfg.minus, &base).context("Couldn't load Minus image.")?,
    );
    r.insert(
        Zl,
        ButtonDisplay::from_cfg(&cfg.zl, &base).context("Couldn't load Zl image.")?,
    );
    r.insert(
        Zr,
        ButtonDisplay::from_cfg(&cfg.zr, &base).context("Couldn't load Zr image.")?,
    );
    r.insert(
        L,
        ButtonDisplay::from_cfg(&cfg.l, &base).context("Couldn't load L image.")?,
    );
    r.insert(
        R,
        ButtonDisplay::from_cfg(&cfg.r, &base).context("Couldn't load R image.")?,
    );
    r.insert(
        Up,
        ButtonDisplay::from_cfg(&cfg.up, &base).context("Couldn't load Up image.")?,
    );
    r.insert(
        Down,
        ButtonDisplay::from_cfg(&cfg.down, &base).context("Couldn't load Down image.")?,
    );
    r.insert(
        Left,
        ButtonDisplay::from_cfg(&cfg.left, &base).context("Couldn't load Left image.")?,
    );
    r.insert(
        Right,
        ButtonDisplay::from_cfg(&cfg.right, &base).context("Couldn't load Right image.")?,
    );
    r.insert(
        Ls,
        ButtonDisplay::from_cfg(&cfg.ls, &base).context("Couldn't load Ls image.")?,
    );
    r.insert(
        Rs,
        ButtonDisplay::from_cfg(&cfg.rs, &base).context("Couldn't load Rs image.")?,
    );
    r.insert(
        Lsl,
        ButtonDisplay::from_cfg(&cfg.lsl, &base).context("Couldn't load Lsl image.")?,
    );
    r.insert(
        Lsr,
        ButtonDisplay::from_cfg(&cfg.lsr, &base).context("Couldn't load Lsr image.")?,
    );
    r.insert(
        Rsl,
        ButtonDisplay::from_cfg(&cfg.rsl, &base).context("Couldn't load Rsl image.")?,
    );
    r.insert(
        Rsr,
        ButtonDisplay::from_cfg(&cfg.rsr, &base).context("Couldn't load Rsr image.")?,
    );
    Ok(r)
}

impl ButtonDisplay {
    fn from_cfg(cfg: &ConfigButton, base: &Path) -> Result<Self> {
        Ok(Self {
            pos: cfg.pos,
            tex: if cfg.image.is_empty() {
                default_image()
            } else {
                load_image(base.join(&cfg.image))?
            },
        })
    }
}

pub struct Skin {
    pub background: Texture2D,
    pub stick_mode: StickMode,
    pub players: [PlayerSkin; 8],
}

pub struct PlayerSkin {
    pub buttons: HashMap<ButtonType, ButtonDisplay>,
    pub ls: Stick,
    pub rs: Stick,
}

#[derive(PartialEq, Eq, Hash, Deserialize, Clone, Copy, Debug)]
pub enum ButtonType {
    A,
    B,
    X,
    Y,
    Plus,
    Minus,
    Zl,
    Zr,
    L,
    R,
    Up,
    Down,
    Left,
    Right,
    Ls,
    Rs,
    Lsl,
    Lsr,
    Rsl,
    Rsr,
}

pub struct ButtonDisplay {
    pub tex: Texture2D,
    pub pos: Pos,
}

pub struct Stick {
    pub tex: Texture2D,
    pub pos: Pos,
    pub range: f32,
}
