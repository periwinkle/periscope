use crate::{
    config::{def_to_real_kc, Config, KeyCodeDef},
    net::{run_net, ControllerState, NetThreadMsg},
    skin::{bg_dims, ButtonType, PlayerSkin, Skin, StickMode},
    ui::{run_ui, show_error, Data, ErrorRet},
};
use anyhow::Result;
use crossbeam_channel::{unbounded, Receiver, Sender};
use crossbeam_queue::ArrayQueue;
use macroquad::{
    miniquad::conf::{LinuxBackend, Platform},
    prelude::*,
    Window,
};
use std::{
    collections::HashSet,
    sync::Arc,
    time::{Duration, Instant},
};

fn gen_conf(dims: (i32, i32)) -> Conf {
    Conf {
        window_title: String::from("periscope"),
        window_resizable: true,
        window_width: dims.0,
        window_height: dims.1,
        platform: Platform {
            linux_backend: LinuxBackend::X11WithWaylandFallback,
            ..Default::default()
        },
        ..Default::default()
    }
}

pub fn run_viewer(cfg: Config) -> Result<()> {
    let (to_net, from_vwr) = unbounded();
    let (to_vwr, from_net) = unbounded();
    let to_net2 = to_net.clone();
    let q = Arc::new(ArrayQueue::new(1));
    let delay = Duration::from_millis(cfg.delay.unwrap_or(0));
    let h = run_net(Arc::clone(&q), to_vwr.clone(), from_vwr.clone(), delay);
    Window::from_config(gen_conf((400, 200)), async move {
        if let Err(e) = window_loop(cfg, Arc::clone(&q), to_net.clone(), from_net).await {
            eprintln!("{e:?}");
            to_net.send(NetThreadMsg::Exit).unwrap();
            std::process::exit(1);
        }
    });
    println!("Exiting...");
    let _ = to_net2.send(NetThreadMsg::Exit);
    let _ = h.join();
    Ok(())
}

enum Showing {
    ToConfig,
    ConfigUI,
    ToViewer,
    Viewer,
    Error,
}

use Showing::*;

pub static FRAME_TIME: Duration = Duration::from_micros(16_667);

async fn window_loop(
    mut cfg: Config,
    queue: Arc<ArrayQueue<Vec<ControllerState>>>,
    tx: Sender<NetThreadMsg>,
    rx: Receiver<NetThreadMsg>,
) -> Result<()> {
    let mut s = Skin::default();
    let mut cs = vec![ControllerState::default(); 8];
    let mut no_frames = 0;
    let mut what = if cfg.show_config() {
        ConfigUI
    } else {
        ToViewer
    };
    let mut data = Data::new(&mut cfg);
    let mut err = String::new();
    let mut cfg_key = def_to_real_kc(cfg.config_key.unwrap_or(KeyCodeDef::C));
    let mut rel_key = def_to_real_kc(cfg.reload_key.unwrap_or(KeyCodeDef::R));
    let mut timer;
    loop {
        timer = Instant::now();
        clear_background(BLACK);
        match what {
            ToConfig => {
                request_new_screen_size(400.0, 200.0);
                what = ConfigUI;
            }
            ConfigUI => {
                if !run_ui(&mut cfg, &mut data) {
                    what = ToViewer;
                }
            }
            ToViewer => {
                what = Viewer;
                let r = bg_dims(&cfg.skin);
                if let Ok(dims) = r {
                    request_new_screen_size(dims.0 as f32, dims.1 as f32);
                    let r = Skin::open(&cfg.skin);
                    if let Ok(skin) = r {
                        tx.send(NetThreadMsg::StartCapture(cfg.switch_addr.clone()))
                            .unwrap();
                        s = skin;
                        cfg.write()?;
                        cfg_key = def_to_real_kc(cfg.config_key.unwrap_or(KeyCodeDef::C));
                        rel_key = def_to_real_kc(cfg.reload_key.unwrap_or(KeyCodeDef::R));
                    } else if let Err(e) = r {
                        err = e.to_string();
                        what = Error;
                    }
                } else if let Err(e) = r {
                    err = e.to_string();
                    what = Error;
                }
            }
            Viewer => {
                if let Ok(NetThreadMsg::Error(e)) = rx.try_recv() {
                    err = e;
                    what = Error;
                    println!("{err}");
                } else {
                    if let Some(frame) = queue.pop() {
                        cs = frame;
                        no_frames = 0;
                    } else {
                        no_frames += 1;
                    }
                    if no_frames == 60 {
                        cs = vec![ControllerState::default(); 8];
                    }
                    viewer_impl(&s, &cs[..]);
                    if is_key_pressed(cfg_key) {
                        tx.send(NetThreadMsg::StopCapture).unwrap();
                        what = ToConfig;
                    } else if is_key_pressed(rel_key) {
                        tx.send(NetThreadMsg::StopCapture).unwrap();
                        what = ToViewer;
                    }
                }
            }
            Error => {
                let er = show_error(&err);
                match er {
                    ErrorRet::Close => break,
                    ErrorRet::Reload => {
                        tx.send(NetThreadMsg::StopCapture).unwrap();
                        what = ToViewer;
                    }
                    ErrorRet::Config => {
                        tx.send(NetThreadMsg::StopCapture).unwrap();
                        what = ToConfig;
                    }
                    ErrorRet::Wait => {}
                }
            }
        }
        if timer.elapsed() < FRAME_TIME {
            std::thread::sleep(FRAME_TIME - timer.elapsed());
        }
        next_frame().await;
    }
    Ok(())
}

fn viewer_impl(s: &Skin, cs: &[ControllerState]) {
    draw_texture(s.background, 0.0, 0.0, WHITE);
    for (i, state) in cs.iter().enumerate() {
        let lxm = state.ls.x / 32767.0 * s.players[i].ls.range;
        let rxm = state.rs.x / 32767.0 * s.players[i].rs.range;
        let lym = -state.ls.y / 32767.0 * s.players[i].ls.range;
        let rym = -state.rs.y / 32767.0 * s.players[i].rs.range;
        match s.stick_mode {
            StickMode::StickOnTop => {
                draw_buttons(&s.players[i], &state.buttons);
                draw_sticks(&s.players[i], lxm, lym, rxm, rym);
            }
            StickMode::ButtonOnTop => {
                draw_sticks(&s.players[i], lxm, lym, rxm, rym);
                draw_buttons(&s.players[i], &state.buttons);
            }
            StickMode::Unified => {
                for button in state
                    .buttons
                    .iter()
                    .filter(|&&b| b != ButtonType::Ls && b != ButtonType::Rs)
                {
                    let disp = s.players[i].buttons.get(&button).unwrap();
                    draw_texture(disp.tex, disp.pos.x, disp.pos.y, WHITE);
                }
                if state.buttons.contains(&ButtonType::Ls) {
                    draw_texture(
                        s.players[i].buttons.get(&ButtonType::Ls).unwrap().tex,
                        s.players[i].ls.pos.x + lxm,
                        s.players[i].ls.pos.y + lym,
                        WHITE,
                    );
                } else {
                    draw_texture(
                        s.players[i].ls.tex,
                        s.players[i].ls.pos.x + lxm,
                        s.players[i].ls.pos.y + lym,
                        WHITE,
                    );
                }
                if state.buttons.contains(&ButtonType::Rs) {
                    draw_texture(
                        s.players[i].buttons.get(&ButtonType::Rs).unwrap().tex,
                        s.players[i].rs.pos.x + rxm,
                        s.players[i].rs.pos.y + rym,
                        WHITE,
                    );
                } else {
                    draw_texture(
                        s.players[i].rs.tex,
                        s.players[i].rs.pos.x + rxm,
                        s.players[i].rs.pos.y + rym,
                        WHITE,
                    );
                }
            }
        }
    }
}

fn draw_sticks(p: &PlayerSkin, lxm: f32, lym: f32, rxm: f32, rym: f32) {
    draw_texture(p.ls.tex, p.ls.pos.x + lxm, p.ls.pos.y + lym, WHITE);
    draw_texture(p.rs.tex, p.rs.pos.x + rxm, p.rs.pos.y + rym, WHITE);
}

fn draw_buttons(p: &PlayerSkin, buttons: &HashSet<ButtonType>) {
    for button in buttons.iter() {
        let disp = p.buttons.get(&button).unwrap();
        draw_texture(disp.tex, disp.pos.x, disp.pos.y, WHITE);
    }
}
