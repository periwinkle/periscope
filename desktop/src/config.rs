use crate::cli::CommandLine;
use anyhow::Result;
use directories::ProjectDirs;
use macroquad::prelude::KeyCode;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::{fs, io::Write, path::PathBuf};
use toml::{from_str, to_string_pretty};

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Config {
    pub switch_addr: String,
    pub skin: String,
    pub viewer_only: Option<bool>,
    pub delay: Option<u64>,
    pub config_key: Option<KeyCodeDef>,
    pub reload_key: Option<KeyCodeDef>,
}

static DIRS: Lazy<ProjectDirs> = Lazy::new(|| {
    ProjectDirs::from("", "periwinkle", "periscope").expect("No valid home directory found!")
});

const DEFCFG: &str = "switch_addr=\"\"\nskin=\"\"\n";

pub fn config_dir() -> PathBuf {
    DIRS.config_dir().to_path_buf()
}

impl Config {
    pub fn open() -> Result<Self> {
        let p = config_dir();
        let config = p.join("config.toml");
        if !config.exists() {
            fs::create_dir_all(&p)?;
            fs::File::create(&config)?;
        }
        if let Ok(c) = from_str(&fs::read_to_string(&config)?) {
            Ok(c)
        } else {
            fs::File::options()
                .write(true)
                .truncate(true)
                .open(config)?
                .write_all(&DEFCFG.as_bytes())?;
            Ok(Self::default())
        }
    }
    pub fn add_cli(&mut self, cli: CommandLine) {
        if cli.switch_addr.is_some() {
            self.switch_addr = cli.switch_addr.unwrap();
        }
        if !cli.skin.is_empty() {
            self.skin = cli.skin;
        }
        if cli.viewer_only {
            self.viewer_only = Some(true);
        }
        if cli.delay.is_some() {
            self.delay = cli.delay;
        }
    }
    pub fn show_config(&self) -> bool {
        !self.viewer_only.is_some_and(|v| v)
    }
    pub fn write(&self) -> Result<()> {
        let p = config_dir();
        let config = p.join("config.toml");
        if !p.exists() {
            fs::create_dir_all(&p)?;
        }
        fs::File::options()
            .write(true)
            .truncate(true)
            .open(config)?
            .write_all(&to_string_pretty(&self)?.as_bytes())?;
        Ok(())
    }
}

#[repr(usize)]
#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum KeyCodeDef {
    Space,
    Apostrophe,
    Comma,
    Minus,
    Period,
    Slash,
    Key0,
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    Key8,
    Key9,
    Semicolon,
    Equal,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    LeftBracket,
    Backslash,
    RightBracket,
    GraveAccent,
    World1,
    World2,
    Escape,
    Enter,
    Tab,
    Backspace,
    Insert,
    Delete,
    Right,
    Left,
    Down,
    Up,
    PageUp,
    PageDown,
    Home,
    End,
    CapsLock,
    ScrollLock,
    NumLock,
    PrintScreen,
    Pause,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    F13,
    F14,
    F15,
    F16,
    F17,
    F18,
    F19,
    F20,
    F21,
    F22,
    F23,
    F24,
    F25,
    Kp0,
    Kp1,
    Kp2,
    Kp3,
    Kp4,
    Kp5,
    Kp6,
    Kp7,
    Kp8,
    Kp9,
    KpDecimal,
    KpDivide,
    KpMultiply,
    KpSubtract,
    KpAdd,
    KpEnter,
    KpEqual,
    LeftShift,
    LeftControl,
    LeftAlt,
    LeftSuper,
    RightShift,
    RightControl,
    RightAlt,
    RightSuper,
    Menu,
    Unknown,
}

const KC_ARR: [KeyCode; 121] = [
    KeyCode::Space,
    KeyCode::Apostrophe,
    KeyCode::Comma,
    KeyCode::Minus,
    KeyCode::Period,
    KeyCode::Slash,
    KeyCode::Key0,
    KeyCode::Key1,
    KeyCode::Key2,
    KeyCode::Key3,
    KeyCode::Key4,
    KeyCode::Key5,
    KeyCode::Key6,
    KeyCode::Key7,
    KeyCode::Key8,
    KeyCode::Key9,
    KeyCode::Semicolon,
    KeyCode::Equal,
    KeyCode::A,
    KeyCode::B,
    KeyCode::C,
    KeyCode::D,
    KeyCode::E,
    KeyCode::F,
    KeyCode::G,
    KeyCode::H,
    KeyCode::I,
    KeyCode::J,
    KeyCode::K,
    KeyCode::L,
    KeyCode::M,
    KeyCode::N,
    KeyCode::O,
    KeyCode::P,
    KeyCode::Q,
    KeyCode::R,
    KeyCode::S,
    KeyCode::T,
    KeyCode::U,
    KeyCode::V,
    KeyCode::W,
    KeyCode::X,
    KeyCode::Y,
    KeyCode::Z,
    KeyCode::LeftBracket,
    KeyCode::Backslash,
    KeyCode::RightBracket,
    KeyCode::GraveAccent,
    KeyCode::World1,
    KeyCode::World2,
    KeyCode::Escape,
    KeyCode::Enter,
    KeyCode::Tab,
    KeyCode::Backspace,
    KeyCode::Insert,
    KeyCode::Delete,
    KeyCode::Right,
    KeyCode::Left,
    KeyCode::Down,
    KeyCode::Up,
    KeyCode::PageUp,
    KeyCode::PageDown,
    KeyCode::Home,
    KeyCode::End,
    KeyCode::CapsLock,
    KeyCode::ScrollLock,
    KeyCode::NumLock,
    KeyCode::PrintScreen,
    KeyCode::Pause,
    KeyCode::F1,
    KeyCode::F2,
    KeyCode::F3,
    KeyCode::F4,
    KeyCode::F5,
    KeyCode::F6,
    KeyCode::F7,
    KeyCode::F8,
    KeyCode::F9,
    KeyCode::F10,
    KeyCode::F11,
    KeyCode::F12,
    KeyCode::F13,
    KeyCode::F14,
    KeyCode::F15,
    KeyCode::F16,
    KeyCode::F17,
    KeyCode::F18,
    KeyCode::F19,
    KeyCode::F20,
    KeyCode::F21,
    KeyCode::F22,
    KeyCode::F23,
    KeyCode::F24,
    KeyCode::F25,
    KeyCode::Kp0,
    KeyCode::Kp1,
    KeyCode::Kp2,
    KeyCode::Kp3,
    KeyCode::Kp4,
    KeyCode::Kp5,
    KeyCode::Kp6,
    KeyCode::Kp7,
    KeyCode::Kp8,
    KeyCode::Kp9,
    KeyCode::KpDecimal,
    KeyCode::KpDivide,
    KeyCode::KpMultiply,
    KeyCode::KpSubtract,
    KeyCode::KpAdd,
    KeyCode::KpEnter,
    KeyCode::KpEqual,
    KeyCode::LeftShift,
    KeyCode::LeftControl,
    KeyCode::LeftAlt,
    KeyCode::LeftSuper,
    KeyCode::RightShift,
    KeyCode::RightControl,
    KeyCode::RightAlt,
    KeyCode::RightSuper,
    KeyCode::Menu,
    KeyCode::Unknown,
];

pub fn def_to_real_kc(def: KeyCodeDef) -> KeyCode {
    let idx = def as usize;
    KC_ARR[idx]
}
